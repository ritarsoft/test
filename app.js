/**
 * We2o Mobile application main module.
 *
 * @author Umed Khudoiberdiev <info@zar.tj>
 * @copyright weeeteeoo inc.
 */
(function() {
    'use strict';

    /**
     * @ngdoc module
     * @name we2o
     */
    angular.module('we2o', [

        // vendor dependencies
        'ngCookies', 'ui.bootstrap', 'ui.router', 'chieffancypants.loadingBar',
        'vcRecaptcha', 'blockUI', 'ngAnimate', 'ngNumeraljs', 'angular-carousel',
        'ngTouch', 'autoGrowInput', 'ngSanitize', 'angularMoment', 'ngMessages',

        // library dependencies
        'session', 'oauth', 'commonsMod', 'notificationCenterMod', 'symfonyProfiler', 'eventDispatcher',
        'angur', 'masterKeyMod', 'errorPagesMod', 'textTruncate', 'formUtils', 'modelUtils',
        'domUtils', 'errorMessageInterceptor', 'youtubePlayer', 'daterangepickerPlugin', 'imagecropPlugin',
        'tooltipsterPlugin', 'countrySelect', 'numberInput', 'toggleSwitch', 'uiBlock', 'formExceptionHandler',
        'fadeoutCarusel', 'dragAndDrop', 'editInPlace', 'momentFilters', 'commonFilters',

        // we2o commons dependencies
        'facebookApiHelper', 'twitterApiHelper', 'linkedInApi', 'googleApi', 'we2oRating', 'we2oJsCommons', 'onepageScroll',

        // module dependencies
        'charityMod', 'donationMod', 'companyMod', 'userMod', 'mainMod', 'statisticsMod', 'searchMod', 'mailMod', 'adminMod',
        'twitterChannelMod', 'socialWallMod', 'socialConnectMod', 'reviewMod', 'fixtureMod', 'googleAnalyticsMod',
        'pdfExportMod', 'payrollMod', 'csvExportMod', 'employeeMod', 'feedbackMod', 'dmyDatepicker', 'contractFormMod'
    ])
        .config(configHtml5Mode)
        .config(configOAuth)
        .config(configAngur)
        .config(configDefaultState)
        .config(configUiBlock)
        .config(configMasterKey)
        .config(configSymfonyProfiler)
        .config(configVersion)
        .config(configFacebookApi)
        .run(runAndConfigSession)
        .run(runUiRouter)
        .run(runAnalytics)
        .run(runSocials)
        .run(closeOpenedModalsOnRouteChange)
        .run(refreshCurrentUserOnDonation)
        .run(loadCharitiesOnce);

    /**
     * @ngInject
     */
    function configHtml5Mode($locationProvider) {
        $locationProvider.html5Mode(true);

        /* BC for browsers that don't support html5 mode */
        $locationProvider.hashPrefix('#');
    }

    /**
     * @ngInject
     */
    function configOAuth(OAuthManagerProvider, OAuthRequestHeadersInterceptorProvider, OAuthTokenRefreshInterceptorProvider, app_parameters) {

        OAuthManagerProvider.setOAuthCredentials(app_parameters.oauth_client_id, app_parameters.oauth_client_secret);
        OAuthRequestHeadersInterceptorProvider.setAllowedUrl(app_parameters.rest_server);

        OAuthTokenRefreshInterceptorProvider.setAuthenticationFailedCallback(function(injector) {
            var logoutPromise = injector.get('UserManager').resetSession();
            logoutPromise.finally(function() {
                injector.get('$state').go('main.home.login');
            });
            return logoutPromise;
        });
    }

    /**
     * @ngInject
     */
    function configAngur(AngurProvider, app_parameters) {
        AngurProvider.setBaseUrl(app_parameters.rest_server);
    }

    /**
     * @ngInject
     */
    function configDefaultState($urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
    }

    /**
     * @ngInject
     */
    function configUiBlock(blockUIConfig) {
        blockUIConfig.autoBlock = false;
        blockUIConfig.template = '<div ng-show="state.blockCount > 0" class="block-ui-overlay" ng-class="{ \'block-ui-visible\': state.blocking }"></div><div ng-show="state.blocking" class="block-ui-message-container"><div class="block-ui-message"><img ng-src="{{ state.message }}" /></div></div>';
        blockUIConfig.message = 'images/Preloader_4_trans.gif';
        blockUIConfig.delay = 1;
    }

    /**
     * @ngInject
     */
    function configMasterKey(MasterKeyConfigurationProvider, app_parameters) {
        MasterKeyConfigurationProvider.setAllowedUrl(app_parameters.rest_server);
    }

    /**
     * @ngInject
     */
    function configSymfonyProfiler(SymfonyProfilerConfigurationProvider, app_parameters) {
        SymfonyProfilerConfigurationProvider.setEnabled(app_parameters.debug_mode);
    }

    /**
     * @ngInject
     */
    function configVersion(MainAppVersionManagerProvider, app_configuration, app_parameters) {
        MainAppVersionManagerProvider.setVersionShowEnabled(app_parameters.show_app_version);
        MainAppVersionManagerProvider.setLocalVersion(app_configuration.version);
    }

    /**
     * @ngInject
     */
    function configFacebookApi(FacebookApiHelperConfigProvider, app_parameters) {
        FacebookApiHelperConfigProvider.setAppId(app_parameters.facebook_app_id);
    }

    /**
     * @ngInject
     */
    function runAndConfigSession(SessionManager, $state, UserRoleUtils, CurrentUserManager) {

        SessionManager.setRequiredAuthorizationFailCallback(function() {
            $state.go('main.home.login');
        });

        SessionManager.setCheckUserRolesCallback(function(user, roles, useAnd) {
            return UserRoleUtils.hasUserRoles(user, roles, useAnd);
        });

        SessionManager.setRequestUserReloadCallback(function() {
            return CurrentUserManager.load();
        });
    }

    /**
     * @ngInject
     */
    function runUiRouter($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }

    /**
     * @ngInject
     */
    function runSocials($gapi, GoogleApi, app_parameters) {
        GoogleApi.init(app_parameters.google_clent_id);
        $gapi.get().then(function(gapi) {
            // gapi.client.setApiKey(app_parameters.google_api_key);
            gapi.client.setApiKey('AIzaSyAnpbyFs0taJEqr0QKqOHvZEP7lQilhT_0');
        });
    }

    /**
     * @ngInject
     */
    function runAnalytics(GoogleAnalyticsSubscriber) {
        GoogleAnalyticsSubscriber.register();
    }

    /**
     * @ngInject
     */
    function closeOpenedModalsOnRouteChange($rootScope, $modalStack) {
        $rootScope.$on('$stateChangeStart', function (newVal, oldVal) {
            if (oldVal !== newVal)
                $modalStack.dismissAll();
        });
    }

    /**
     * @ngInject
     */
    function refreshCurrentUserOnDonation(EventDispatcher, CurrentUserManager) {
        EventDispatcher.on('DONATION_SUCCESS', function() {
            CurrentUserManager.load();
        });
    }

    /**
     * Here we load charities and charity categories are they are cached internally and will be accessible
     * to all directives without being reloaded.
     *
     * @ngInject
     */
    function loadCharitiesOnce(CharityRepository, CharityCategoryRepository, ErrorPagesRedirector) {

        CharityRepository.getList().error(function() {
            ErrorPagesRedirector.serverError();
        });

        CharityCategoryRepository.getList().error(function() {
            ErrorPagesRedirector.serverError();
        });
    }
})();
