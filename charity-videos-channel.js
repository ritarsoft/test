(function() {
    'use strict';

    /**
     * @ngdoc directive
     * @name charityVideosChannel
     * @restrict E
     * @description
     * Add video list from youtube channel with gapi
     * @param {string} channel The name of youtube channel
     */

     angular.module('charityMod').directive('charityVideosChannel', charityVideosChannel);

    /**
     * @ngdoc controller
     * @name CharityVideosChannelCtrl
     */
    angular.module('charityMod').controller('CharityVideosChannelCtrl', CharityVideosChannelCtrl);

    function charityVideosChannel() {
        return {
            scope: {
                channel: '='
            },
            restrict: 'E',
            templateUrl: 'views/charity/common/charity-videos-channel.html',
            controller: 'CharityVideosChannelCtrl'
        };
    }

    /**
     * @ngInject
     */
    function CharityVideosChannelCtrl($scope, $timeout, $q, GoogleApi) {
        
        // ---------------------------------------------------------------------
        // Local functions
        // ---------------------------------------------------------------------

        var loadChannelId = function(channeName) {
            return GoogleApi.getchannelId(channeName).then(function(response) {
                return response;
            });
        }

        var loadVideosList = function(channelId, quantityVideo) {
            return GoogleApi.getVideos(channelId, quantityVideo).then(function(item) {
                var videos = [];
                _.each(item, function(value) {
                    if(value.snippet.resourceId.videoId) {
                        videos.push(value.snippet.resourceId.videoId);
                    }
                });
                return videos;
            });
        };

        var showVideo = function(ChannelName) {
            var channelIdPromise = loadChannelId(ChannelName);
            
            channelIdPromise.then(function(channelId) {
                loadVideosList(channelId, 3).then(function(response) {
                    $scope.videos = angular.copy(response);
                },function(error) {
                    throw 'Server error. Wrong data.';
                });
            },function(error) {
                loadVideosList(ChannelName, 3).then(function(response) {
                    $scope.videos = angular.copy(response);
                },function(error) {
                    throw 'Server error. Wrong data.';
                });
            });

        }

        // ---------------------------------------------------------------------
        // Scope variables
        // ---------------------------------------------------------------------

        $scope.disabled = false;
        $scope.videos = [];

        // ---------------------------------------------------------------------
        // Scope functions
        // ---------------------------------------------------------------------

        $scope.videoPauseAll = function() {
            var iFrame = angular.element('.slidevideo iframe');
            angular.forEach(iFrame, function(value) {
                value.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
            });

            return true;
        };

        $scope.makeDisabled = function() {
            $scope.disabled = true;
            $timeout(function() {
                $scope.disabled = false;
            }, 500);
        };

        // ---------------------------------------------------------------------
        // Watchers
        // ---------------------------------------------------------------------

        $scope.$watch('channel', function (channel) {
            showVideo(channel);
        });

        // ---------------------------------------------------------------------
        // init
        // ---------------------------------------------------------------------

        showVideo($scope.channel);
    }
})();
