/**
 * Manages connections to Google.
 *
 * @author Alexey Rak <rakalexey@gmail.com>
 * @copyright weeeteeoo inc.
 */
(function () {
    'use strict';

    /**
     * @ngdoc module
     * @name googleApi
     * @description
     * Module for let users connect to their google account.
     */
    angular.module('googleApi', []);

    /**
     * @ngdoc service
     * @name $gapi
     */
    angular.module('googleApi').factory('$gapi', Gapi);

    /**
     * @ngdoc service
     * @name GoogleApi
     */
    angular.module('googleApi').factory('GoogleApi', GoogleApi);

    /**
     * @ngInject
     * @constructor
     */
    function Gapi($window, $q) {
        var deferred = $q.defer();

        $window.googleOnLoadCallback = function () {
            deferred.resolve($window.gapi);
        };

        return {
            get: function() {
                return deferred.promise;
            }
        };
    }

    /**
     * @ngInject
     * @constructor
     */
    function GoogleApi($q, $gapi, app_parameters) {
        var googleInfo, token;

        var reset = function () {
            googleInfo = {
                client_id: false,
                scope: 'https://www.googleapis.com/auth/plus.login'
            };
            token = {};
        };
        reset();

        return {

            /**
             * Configuring google info for a future work.
             *
             * @param {string} clientId
             * @param {string} scope
             */
            init: function(clientId, scope) {
                googleInfo.client_id = clientId || googleInfo.client_id;
                googleInfo.scope = scope || googleInfo.scope;
            },

            /**
             * Destroying of the app
             */
            destroy: function() {
                reset();
            },

            /**
             * Trying to login User automatically.
             * Rejecting if users approve needed first.
             *
             * @returns {promise}
             */
            isAuthorized: function() {
                var deferred = $q.defer();
                var config = { immediate: true };
                angular.extend(config, googleInfo);

                $gapi.get().then(function(gapi) {
                    gapi.auth.authorize(config, function(response) {
                        return response.error ? deferred.reject(response) : deferred.resolve(response);
                    });

                });
                return deferred.promise;
            },

            /**
             * Trying to login user in a normal flow.
             *
             * @param {boolean} getUserData
             * @returns {promise}
             */
            login: function(getUserData) {
                var deferred = $q.defer();
                var config = { immediate: false };
                angular.extend(config, googleInfo);

                $gapi.get().then(function(gapi) {
                    gapi.auth.authorize(config, function() {
                        if (!getUserData)
                            return deferred.resolve();

                        //get google user id since it's not returned with authentication for some reason..
                        gapi.client.load('oauth2', 'v2', function() {
                            gapi.client.oauth2.userinfo.get().execute(function(response) {
                                return response.error ? deferred.reject(response) : deferred.resolve(response);
                            });
                        });
                    });
                });
                return deferred.promise;
            },

            /**
             * Appending to body iframe with a google account logout link.
             * Google API don't have a logout API
             *
             * @returns {boolean}
             */
            logout: function() {
                var iframe = '<iframe id="logoutframe" src="https://accounts.google.com/logout" style="display: none"></iframe>';
                return !!angular.element('body').append(iframe);
            },
            getchannelId: function(channelName) {
                return $gapi.get().then(function(gapi) {
                    return gapi.client.load('youtube', 'v3').then(function() {
                        return gapi.client.youtube.channels.list({
                            part: 'contentDetails',
                            forUsername: channelName
                        }).then(function(response) {
                            if(response.result.items.length == 0 || !response.result.items[0].contentDetails.relatedPlaylists.uploads) {
                                throw 'Server error. Wrong data.';
                            }
                            return response.result.items[0].contentDetails.relatedPlaylists.uploads;
                        }, function(error){
                            throw 'Server error. Wrong data.';
                        });
                    });    
                });
            },
            getVideos: function(channelId, quantityVideo) {
                return $gapi.get().then(function(gapi) {
                    return gapi.client.load('youtube', 'v3').then(function() {
                        return gapi.client.youtube.playlistItems.list({
                            part: 'snippet',
                            playlistId: channelId,
                            maxResults: quantityVideo
                        }).then(function(response) {
                            if(!response.result.items || response.result.items.error) {
                                throw 'Server error. Wrong data.';
                            }
                            return response.result.items;   
                        }, function(error){
                            throw 'Server error. Wrong data.';
                        });
                    }); 
                });
            }
        };
    }
})();
